import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { stopPropagation } from 'react-updaters';
import {
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  UncontrolledTooltip,
} from 'reactstrap';
import { RaceMetadata, Organization } from '../../common/types';
import EndorsementDisplay from './EndorsementDisplay';

interface Props {
  /** ID suffix to add to Popover DOM IDs */
  tableId: string;
  organization: Organization;
  races: RaceMetadata[];

  /** Which key in the `organization` to use to link to the endorsements */
  linkKey: 'link' | 'propositionsLink';

  /** Map of candidate name to color, for easy browsing */
  colorMap: { [name: string]: string };
}

/**
 * A single row in an endorsement table, representing the endorsements
 * of one organization
 */
export default class EndorsementRow extends React.PureComponent<Props> {
  handleOpenEndorsementsLink = () => {
    window.open(this.props.organization.link);
  };

  render() {
    const { organization, races, tableId, linkKey, colorMap } = this.props;
    const buttonId = `EndorsementRow__${organization.name.replace(
      /\W+/g,
      '',
    )}_${tableId}`;
    const hasReasoningId = `EndorsementRow__${organization.name.replace(
      /\W+/g,
      '',
    )}_${tableId}_hasReasoning`;

    const hasEndorsements = races.some(
      race => organization.endorsements[race.race],
    );
    if (!hasEndorsements) return null;

    return (
      <tr
        key={organization.name}
        className={
          organization.hasReasoning ? 'EndorsementRow__hasReasoning' : ''
        }
        onClick={this.handleOpenEndorsementsLink}
      >
        <td>
          <UncontrolledPopover
            trigger="legacy"
            placement="bottom"
            target={buttonId}
            fade
          >
            <PopoverHeader>{organization.name}</PopoverHeader>
            <PopoverBody>
              <div>{organization.description}</div>
              <div className="mt-2">
                <a
                  href={
                    linkKey === 'link'
                      ? organization.link
                      : organization.propositionsLink
                  }
                  className="btn btn-primary"
                  target="_blank"
                  rel="nofollow"
                >
                  Go to endorsements source
                </a>
              </div>
            </PopoverBody>
          </UncontrolledPopover>
          <button
            className="btn btn-link p-0 font-weight-bold text-left"
            type="button"
            id={buttonId}
            onClick={stopPropagation as any}
          >
            {organization.name}
          </button>
          {organization.hasReasoning && (
            <>
              <UncontrolledTooltip
                target={hasReasoningId}
                delay={0}
                fade={false}
              >
                This organization explains why they endorsed a certain position.
                Click to visit their site.
              </UncontrolledTooltip>
              <a
                href={organization.link}
                className="ml-2 text-info"
                id={hasReasoningId}
              >
                <FontAwesomeIcon icon={faInfoCircle} />
              </a>
            </>
          )}
        </td>
        {races.map(race => {
          const endorsement = organization.endorsements[race.race];
          return (
            <td key={race.race}>
              {endorsement && (
                <EndorsementDisplay
                  endorsement={endorsement}
                  colorMap={colorMap}
                />
              )}
            </td>
          );
        })}
      </tr>
    );
  }
}
