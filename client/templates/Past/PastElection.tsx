import React from 'react';
import MainPageWithHeader from '../MainPageWithHeader';
import ElectionEndorsements, {
  ElectionEndorsementsProps,
} from '../ElectionEndorsements';

/** An archival page for a past (already-happened) election */
const PastElection = React.memo(function PastElection(
  props: ElectionEndorsementsProps,
) {
  const { electionTitle } = props;
  return (
    <MainPageWithHeader>
      <div className="container py-5" id="HomePage__contentStart">
        <h1>Endorsements for the {electionTitle} election</h1>

        <ElectionEndorsements {...props} />
      </div>
    </MainPageWithHeader>
  );
});

export default PastElection;
