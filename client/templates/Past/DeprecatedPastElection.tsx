import React from 'react';
import Endorsements, { EndorsementsProps } from '../Endorsements';
import MainPageWithHeader from '../MainPageWithHeader';

/** An archival page for a past (already-happened) election */
const DeprecatedPastElection = React.memo(function PastElection(
  props: EndorsementsProps,
) {
  const { electionTitle } = props;
  return (
    <MainPageWithHeader>
      <div className="container py-5" id="HomePage__contentStart">
        <h1>Endorsements for the {electionTitle} election</h1>

        <Endorsements {...props} />
      </div>
    </MainPageWithHeader>
  );
});

export default DeprecatedPastElection;
