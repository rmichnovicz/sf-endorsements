import React from 'react';
import electionData from '../../data/Feb2022';
import PastElection from './PastElection';

const electionTitle = 'February, 2022';

/** Results for February 2022 election */
const Feb2022: React.FunctionComponent<{}> = React.memo(function Feb2022() {
  return <PastElection electionTitle={electionTitle} data={electionData} />;
});

export default Feb2022;
