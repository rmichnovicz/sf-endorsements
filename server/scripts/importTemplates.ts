import XLSX from 'xlsx';
import _ from 'lodash';
import prettier from 'prettier';
import { organizations } from '../../common/organizations';
import {
  ElectionData,
  OrganizationEndorsements,
  RaceMetadata,
  Section,
} from '../../common/types';

function camelcaseKeys(obj: Record<string, any>): any {
  return _.zipObject(
    Object.keys(obj).map(k => _.camelCase(k)),
    Object.values(obj),
  );
}

interface RaceMetadataWithNameKey extends RaceMetadata {
  nameKey: string;
  section: string;
}

interface OrganizationMetadata {
  organization: string;
  website: string;
  hasReasoning?: 'Yes';
  hasNonPoliticalActivities?: 'Yes';
}

interface SectionWithoutMetadata extends Omit<Section, 'metadata'> {}

async function run() {
  const file = process.argv[2];
  const workbook = XLSX.readFile(file);

  // Import races
  const racesSheet = workbook.Sheets.Races;
  if (!racesSheet) throw new Error('Could not find Races sheet');

  const races: RaceMetadataWithNameKey[] = (XLSX.utils.sheet_to_json(
    racesSheet,
  ) as Record<string, string>[]).map(item => camelcaseKeys(item));

  // Import organizations metadata
  const orgMetadataSheet = workbook.Sheets.Organizations;
  if (!orgMetadataSheet) throw new Error('Could not find Organizations sheet');
  const orgMetadata: OrganizationMetadata[] = (XLSX.utils.sheet_to_json(
    orgMetadataSheet,
  ) as Record<string, string>[]).map(item => camelcaseKeys(item));

  // Import sections
  const sectionsSheet = workbook.Sheets.Sections;
  if (!sectionsSheet) throw new Error('Could not find Sections sheet');

  const sectionsWithoutMetadata: SectionWithoutMetadata[] = (XLSX.utils.sheet_to_json(
    sectionsSheet,
  ) as Record<string, string>[]).map(item => camelcaseKeys(item));

  // Import endorsements sheet
  const endorsementsSheet = workbook.Sheets.Endorsements;
  if (!endorsementsSheet) throw new Error('Could not find Endorsements sheet');

  /** The raw endorsements sheet looks something like { "Race": "propH", "someOrg": "Yes", ...} */
  const rawEndorsements: {
    [organization: string]: any;
  }[] = XLSX.utils.sheet_to_json(endorsementsSheet);

  // The first few rows are special rows for "Has rationale",
  // "Website link", and "Endorsements link"
  const [
    hasRationaleUntyped,
    _websiteLink,
    endorsementsLinkUntyped,
    ...regularRowsUntyped
  ] = rawEndorsements;
  const hasRationale: { [org: string]: boolean } = hasRationaleUntyped;
  const endorsementsLink: { [org: string]: string } = endorsementsLinkUntyped;
  const regularRows: { [org: string]: string }[] = regularRowsUntyped;

  const orgMetadataWithEndorsements = orgMetadata.filter(
    metadata => endorsementsLink[metadata.organization],
  );

  // Create the organizations in the order they appear in the Organizations
  // spreadsheet
  const organizationEndorsements: {
    [orgName: string]: OrganizationEndorsements;
  } = _.zipObject(
    orgMetadataWithEndorsements.map(metadata => metadata.organization),
    orgMetadataWithEndorsements.map(metadata => ({
      endorsements: {},
      /** Link to the endorsements */
      link: endorsementsLink[metadata.organization],
      hasReasoning: hasRationale[metadata.organization],
      hasNonPoliticalActivities: metadata.hasNonPoliticalActivities === 'Yes',
    })),
  );

  for (const endorsementsForRace of regularRows) {
    const race = endorsementsForRace.Race;
    for (const orgName of Object.keys(endorsementsForRace)) {
      if (orgName === 'Race') continue;

      const matchingOrg = organizations.find(org => org.name === orgName);

      if (!matchingOrg) {
        console.warn('SKIPPED: Could not find', orgName);
        continue;
      }

      const orgEndorsementsStr: string = endorsementsForRace[orgName];
      organizationEndorsements[orgName].endorsements[
        race
      ] = orgEndorsementsStr.includes(',')
        ? orgEndorsementsStr.split(/\s*,\s*/)
        : orgEndorsementsStr;
    }
  }

  // Generate the code: we need 2 things:
  // 1. Section[]
  // 2. OrganizationEndorsements[]
  const racesBySection = _.groupBy(races, r => r.section);
  const sections = sectionsWithoutMetadata.map(section => ({
    ...section,
    metadata: racesBySection[section.heading].map(
      ({ nameKey, section, ...race }): RaceMetadata => race,
    ),
  }));

  const data: ElectionData = {
    sections,
    endorsements: organizationEndorsements,
  };

  const code = `
import { ElectionData } from '../../common/types';

const electionData: ElectionData = ${JSON.stringify(data, null, 2)};
export default electionData;
`;
  console.log(
    prettier
      .format(code, {
        parser: 'babel',
        singleQuote: true,
        trailingComma: 'all',
      })
      .trim(),
  );
}

run();
