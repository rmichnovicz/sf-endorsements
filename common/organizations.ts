import { BaseOrganization, OrgType } from './types';

export const organizations: BaseOrganization[] = [
  {
    name: 'San Francisco Chronicle',
    description:
      'Major San Francisco Bay Area daily newspaper with the largest average daily circulation',
    type: OrgType.newspaper,
    homepage: 'https://www.sfchronicle.com/',
  },
  {
    name: 'San Francisco Examiner',
    type: OrgType.newspaper,
    description: 'Free San Francisco daily newspaper published since 1863',
    homepage: 'https://www.sfexaminer.com/',
  },
  {
    name: 'Democratic Party (SF)',
    type: OrgType.party,
    description:
      'Local branch of the Democratic Party, the dominant party in San Francisco and the more liberal/left-leaning of the two major US parties',
    homepage: 'https://www.sfdemocrats.org/',
  },
  {
    name: 'San Francisco Bay Guardian',
    type: OrgType.newspaper,
    description:
      'Online-only newspaper that reports and promotes left-wing, progressive issues',
    homepage: 'http://www.sfbg.com/',
  },
  {
    name: 'Sierra Club',
    type: OrgType.nonProfit,
    description:
      "The San Francisco Bay Chapter is the local branch of the Sierra Club, America's largest and most effective grassroots environmental organization",
    homepage: 'https://www.sierraclub.org/san-francisco-bay',
  },
  {
    name: 'Green Party (SF)',
    type: OrgType.party,
    description:
      'SF chapter of the national Green Party: part of the Green movement that works to encourage non-violence, ecological respect, grassroots democracy, and social justice',
    homepage: 'https://www.sfgreenparty.org/',
  },
  {
    name: 'YIMBY Action',
    type: OrgType.nonProfit,
    description:
      'Pro-housing advocates trying to increase supply of housing and bring down cost of living',
    homepage: 'https://yimbyaction.org/',
  },
  {
    name: 'SF Tenants Union',
    type: OrgType.nonProfit,
    description:
      'Non-profit that advocates for and educates renters and tenants on their rights',
    homepage: 'https://www.sftu.org/',
  },
  {
    name: 'SF Housing Action Coalition',
    type: OrgType.nonProfit,
    description:
      'Pro-housing organization advocating for building new well-designed, well-located housing at all levels of affordability',
    homepage: 'https://www.sfhac.org/',
  },
  {
    name: 'SF League of Pissed Off Voters',
    type: OrgType.nonProfit,
    description:
      'A group of political enthusiasts formed with the goal of building a progressive majority in San Francisco government',
    homepage: 'http://www.theleaguesf.org/',
  },
  {
    name: 'GrowSF',
    type: OrgType.nonProfit,
    description:
      'Group of volunteers who want San Francisco to invest in growth, with focus on more housing, better transit, and reducing homelessness',
    homepage: 'https://growsf.org/',
  },
  {
    name: 'SF Labor Council',
    type: OrgType.union,
    description:
      'Local body of the AFL-CIO labor union federation: consists of 150 unions representing 100,000 members in the Bay Area',
    homepage: 'http://sflaborcouncil.org/',
  },
  {
    name: 'SEIU 1021',
    type: OrgType.union,
    description:
      'Local chapter of the Service Employers International Union, representing 60,000 members in Northern California',
    homepage: 'https://www.seiu1021.org/',
  },
  {
    name: 'SF League of Conservation Voters',
    type: OrgType.nonProfit,
    description:
      'Non-profit that seeks to protect and improve the local environment, including public transit, public spaces, natural resources, and sustainability',
    homepage: 'https://www.sflcv.org/',
  },
  {
    name: "SF Women's Political Committee",
    type: OrgType.nonProfit,
    description:
      "Largest women's organization in SF that promotes women in leadership, gender parity, and women's issues",
    homepage: 'https://sfwpc.org/',
  },
  {
    name: 'SF Berniecrats',
    type: OrgType.nonProfit,
    description:
      'Group founded by Bernie Sanders supporters from the 2016 election to support a populist, progressive movement on the Left',
    homepage: 'https://sfberniecrats.com/',
  },
  {
    name: 'SPUR (Planning / Urban Research Association)',
    type: OrgType.nonProfit,
    description:
      'A civic planning organization that promotes good urban planning (affordable housing, controlled sprawl, better transit) in the Bay Area',
    homepage: 'https://www.spur.org/',
  },
  {
    name: 'League of Women Voters of SF',
    type: OrgType.nonProfit,
    description:
      'Organization that encourages voting and informed participation in politics: formed originally as part of suffrage movement in early 1900s',
    homepage: 'https://lwvsf.org/',
  },
  {
    name: 'SF Bicycle Coalition',
    type: OrgType.nonProfit,
    description:
      'Bicycle education and advocacy group that aims to make San Francisco a more bicycle-friendly city',
    homepage: 'https://sfbike.org/',
  },
  {
    name: 'Tech Workers Voter Guide',
    type: OrgType.voterGuide,
    description:
      'Voter guide written by a small group of tech voters in San Francisco',
    homepage: 'https://www.techworkers.vote/',
  },
  {
    name: 'SF Republican Party',
    type: OrgType.party,
    description:
      'Local branch of the Republican Party, the more conservative/right-leaning of the two major US parties',
    homepage: 'http://www.sfgop.org/',
  },
  {
    name: 'District 11 Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club representing SF District 11, covering Excelsior, Ocean View, Ingleside, and Visitacion Valley',
    homepage: 'https://www.phdemclub.org/',
  },
  {
    name: 'Potrero Hill Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club founded in 2007 representing Potrero Hill',
    homepage: 'https://www.phdemclub.org/',
  },
  {
    name: 'Harvey Milk LGBTQ Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club that supports LGBTQ and progressive issues and people of marginalized backgrounds',
    homepage: 'http://www.milkclub.org/',
  },
  {
    name: 'United Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club founded in 2016 that represents a diverse group of Democrats advancing goals of social justice, protecting the vulnerable, and providing economic opportunity for all',
    homepage: 'https://www.uniteddems.org/',
  },
  {
    name: 'Ed M. Lee Asian Pacific Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club that hopes to engage Asian Pacific Islanders (API) and support API leaders',
    homepage: 'https://www.edleedems.org/',
  },
  {
    name: 'Alice B. Toklas LGBT Democratic Club',
    type: OrgType.nonProfit,
    description:
      'First LGBT-centered Democratic Party club in the country; advocates for LGBT issues and candidates',
    homepage: 'http://www.alicebtoklas.org/about/',
  },
  {
    name: 'Bernal Heights Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Club founded in 1988 in the Bernal Heights neighborhood of San Francisco',
    homepage: 'http://bhdemocrats.org/',
  },
  {
    name: 'Chinese American Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Club established in 1958 that advances causes of the Chinese American community',
    homepage: 'http://bhdemocrats.org/',
  },
  {
    name: 'Eastern Neighborhoods Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Club established by certain residents of eastern SF -- Districts 3, 6, and 10 (North Beach through FiDi/SOMA, Potrero, and Bayview/Hunters Point)',
    homepage: 'http://bhdemocrats.org/',
  },
  {
    name: 'SF Young Democrats',
    type: OrgType.nonProfit,
    description:
      'Democratic Club that represents progressive issues of young people in San Francisco founded in 1947',
    homepage: 'https://www.sfyd.org/',
  },
  {
    name: 'SF Young Republicans',
    type: OrgType.nonProfit,
    description:
      'Community of Republican Party supporters age 18-40 in San Francisco',
    homepage: 'https://www.sfyd.org/',
  },
  {
    name: 'Noe Valley Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Club established by certain residents of the Noe Valley neighborhood',
    homepage: 'https://www.noevalleydemocrats.org/',
  },
  {
    name: 'Home Sharers Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Club focused on promoting policies that help home-sharing/short-term rental (e.g., Airbnb) hosts and guess',
    homepage: 'https://www.homesharersdemclub.org/',
  },
];
