export type UnpackPromise<T> = T extends Promise<infer U> ? U : T;

export interface SuccessResponse {
  success: true;
  messages: React.ReactNode[];
  data?: any;
}

export interface FailedResponse {
  success: false;
  messages: React.ReactNode[];
  errTypes: string[];
}

export type Response = SuccessResponse | FailedResponse;

export interface ClientConfig {
  shouldServerSideRender: boolean;
  isProduction: boolean;
  googleAnalyticsId: string | null;
}

export type Endorsement = string | string[];

export enum OrgType {
  newspaper = 'newspaper',
  union = 'union',
  nonProfit = 'nonProfit',
  party = 'party',
  voterGuide = 'voterGuide',
}

/**
 * An organization without any extra data specific to the
 * current election
 */
export interface BaseOrganization {
  name: string;
  description: string;
  type: OrgType;
  homepage: string;
}

export interface OrganizationEndorsements {
  /** Link to the endorsements */
  link: string;
  /**
   * Legacy field: the link to the propositions endorsements page.
   * Eliminated because it's almost always just `link`
   */
  propositionsLink?: string;
  endorsements: { [race: string]: Endorsement };

  hasReasoning: boolean;
  hasNonPoliticalActivities: boolean;
}

export interface Organization
  extends BaseOrganization,
    OrganizationEndorsements {}

export interface RaceMetadata {
  race: string;
  name: string;
  title?: string;
  description?: string;
  link?: string;
}

export interface Section {
  heading: string;
  subtitle?: string;
  metadata: RaceMetadata[];
}

/** All the data that should change for a single election */
export interface ElectionData {
  sections: Section[];
  endorsements: { [orgName: string]: OrganizationEndorsements };
}
